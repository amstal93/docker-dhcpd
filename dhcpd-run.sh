#!/bin/bash

DHCP_CONF_DIR=/etc/dhcp \
DHCP_LEASES=/var/lib/dhcp/ 

create_leases() {
	mkdir $DHCP_LEASES
}

copy_leases() {
	cp dhcpd.leases $DHCP_LEASES 
}

copy_conf() {
	cp -r  conf/* $DHCP_CONF_DIR
}

if [ ! -d "$DHCP_LEASES" ]
then
	echo "create leases"
	create_leases
fi

if [ ! -f "$DHCP_LEASES/dhcpd.leases" ]
then
        echo "copy leases"
        copy_leases
fi

if [ ! -f "$DHCP_CONF_DIR/dhcpd.conf" ]
then
        echo "copy conf"
        copy_conf
fi



docker run -d --name dhcpd --net macvlan1 --ip 192.168.0.254 --restart always \
	-v $DHCP_CONF_DIR:$DHCP_CONF_DIR \
	-v $DHCP_LEASES:$DHCP_LEASES \
	 dedmin/dhcpd
