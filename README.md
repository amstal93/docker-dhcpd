# Introduction

This is automated build of dhcpd for runing in docker container


# dhcpd

The Internet Systems Consortium DHCP Server, dhcpd, implements the Dynamic Host Configuration Protocol (DHCP) and the Internet Bootstrap Protocol (BOOTP). DHCP allows hosts on a TCP/IP network to request and be assigned IP addresses, and also to discover information about the network to which they are attached

# Building

Run it in command line:

```bash
docker build -t dedmin/dhcpd https://gitlab.com/dedmin/dhcpd.git
```


# Configuration

Run container on the host system and mount directories and configuration file:

```bash
wget 'https://gitlab.com/dedmin/dhcpd/raw/master/dhcpd-run.sh'
bash dhcpd-run.sh
```