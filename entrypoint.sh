#!/bin/bash

# Entrypoint for docker dhcpd container

create_leases() {
	touch $DHCP_LEASES
}

if [ ! -f "$DHCP_LEASES" ]
then
	echo "create leases"
	create_leases
fi


echo "start dhcpd"
dhcpd --no-pid -4 -f
bash