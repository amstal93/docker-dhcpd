FROM ubuntu

MAINTAINER https://gitlab.com/dedmin

ENV DHCP_CONF_DIR=/etc/dhcp \
    DHCP_LEASES=/var/lib/dhcp \
    TZ=Europe/Moscow

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
apt-get install --no-install-recommends -y isc-dhcp-server \
    tzdata \
    net-tools && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    ln -sf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone

COPY conf/ $DHCP_CONF_DIR/
COPY entrypoint.sh /usr/local/bin
copy dhcpd.leases $DHCP_LEASES



RUN  chmod 755 /usr/local/bin/entrypoint.sh

    

EXPOSE 67/udp

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
